1
2
3
4
5
6
7
8
9
10
11
12
13
14
15
16
17
18
19
20
21
22
23
24
25
26
27
28
29
30
31
32
33
34
35
36
37
#ifndef  RandomNumber_H
#define RandomNumber_H
 
 
#include <vector>
typedef vector<double> Layer;
 
 
class RandomNumber{
 
    friend class GeneticAlgorithmn;
 
protected:
 
      double oldrand[55];   /* Array of 55 random numbers */
      int jrand;                 /* current random number */
      double rndx1, rndx2;    /* used with random normal deviate */
      int rndcalcflag; /* used with random normal deviate */
 
public:
    void   initrandomnormaldeviate() ;
 
    void   warmup_random(double random_seed) ;
 
//---------------------------------------------------------
    double  noise(double mu ,double sigma);
    double randomnormaldeviate();
    double   randomperc();
    void  advance_random() ;
    int  flip(double prob) ;
    void   randomize(double seed);
 
};
 
 
 
#endif